#pragma once
// TCPServer
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>

using namespace std;

#pragma comment (lib,"ws2_32.lib")


class Socket {
public:
	SOCKET _socket;
	bool on = true;

	void create(const char* ip = "127.0.0.1", unsigned short port = 27015) {
		_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}

	//bind 
	bool _bind(sockaddr_in& serverAddress) {
		// bind the socket
		if (bind(_socket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
			cerr << "Bind() failed" << endl;
			close();
			return false;
		}
		cout << "TCP/IP socket bound.\n";
	}

	//listen
	bool _listen(int numClients = 10) {
		if (listen(_socket, numClients * 2) == SOCKET_ERROR) {
			cerr << "Error listening on socket\n";
			close();
			return false;
		}
		return true;
	}

	//connect
	bool _connect(sockaddr_in& serverAddress) {
		return connect(_socket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR;
	}

	//accept
	SOCKET _accept() {
		return accept(_socket, NULL, NULL);
	}

	//bytesRecv = recv(s2._socket, recvbuf, i, 0);
	//receive, return the bytes received
	void receive(vector<string> cmdPatterns = { "" }) {
		while (on) {
			int i;
			int bytesRecv = recv(_socket, (char*)&i, 4, 0);
			cout << "i = " << i << endl;
			//drop1
			if (i == -1 || bytesRecv == -1)
			{
				break;
				//TODO drop??
			}

			char* recvbuf = new char[i];
			bytesRecv = recv(_socket, recvbuf, i, 0);
			cout << "Recv = " << bytesRecv << ": " << recvbuf << ";" << endl;
			char cmd[4] = { 'c','m','d' };
			if (bytesRecv >= 4
				&& recvbuf[0] == cmd[0]
				&& recvbuf[1] == cmd[1]
				&& recvbuf[2] == cmd[2]
				) {
				string str(recvbuf);
				cout << "*** command caught: '" << str << "'" << endl;
			}
			delete[]recvbuf;
		}
	}

	//send
	void _send(string& msg) {
		int msgLngh = 0;
		msgLngh = msg.length() + 1;

		send(_socket, (char*)&msgLngh, sizeof(msgLngh), 0);

		char* sendbuf = new char[msgLngh];

		strcpy_s(sendbuf, msg.length() + 1, msg.c_str());	// or pass &s[0]
		//cout << "sendbuf = " << sendbuf << endl;
		//cout << "strlen(sendbuf) + 1 = " << strlen(sendbuf) + 1 << endl;
		int bytesSent = send(_socket, sendbuf, strlen(sendbuf) + 1, 0);

		delete[]sendbuf;
	}


	void _sendDrop() {
		int i = -1;
		send(_socket, (char*)&i, sizeof(i), 0);
	}

	//operators
	void operator =(SOCKET rhs) {
		_socket = rhs;
	}

	bool operator ==(SOCKET rhs) {
		return _socket == rhs;
	}



	//stop listen
	//close
	void close() {
		closesocket(_socket);
		WSACleanup();
	}


	//bytesRecv = recv(s2._socket, recvbuf, i, 0);
	//receive, return the bytes received
	void receive_client() {
		while (on) {
			int i;
			int bytesRecv = recv(_socket, (char*)&i, 4, 0);
			//drop1
			if (i == -1 || bytesRecv == -1)
			{
				break;
				//TODO drop??
			}

			char* recvbuf = new char[i];
			bytesRecv = recv(_socket, recvbuf, i, 0);
			cout <<  recvbuf << endl;
			delete[]recvbuf;
		}
	}
};
