#include"TCPServer.hpp"



int main(int argc, char** argv){

	string ip = "127.0.0.1";

	if (argc >= 2) {
		ip = argv[1];
	}

	TCPServer server(ip);

	server.startListeningSingle();
}