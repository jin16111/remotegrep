//// TCPServer
//#define _WINSOCK_DEPRECATED_NO_WARNINGS
//#include <WinSock2.h>
//#include <WS2tcpip.h>
//#include <iostream>
//using namespace std;
//
//#pragma comment (lib,"ws2_32.lib")
//
//unsigned short constexpr PORT = 27015;
//
//int main() {
//	cout << "TCPServer\n";
//
//	// initialize WSA
//	WSAData wsaData;
//	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
//	if (iResult != 0) {
//		cerr << "WSAStartup failed: " << iResult << endl;
//		return EXIT_FAILURE;
//	}
//
//
//	// Create the TCP socket
//	SOCKET hSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
//
//	// Create the server address
//	sockaddr_in serverAddress = { 0 };
//	serverAddress.sin_family = AF_INET;
//	serverAddress.sin_port = htons(PORT);
//	//serverAddress.sin_addr.s_addr = inet_addr("127.0.0.1"); // old
//	inet_pton(AF_INET, "127.0.0.1", &(serverAddress.sin_addr));
//
//	// bind the socket
//	if (bind(hSocket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
//		cerr << "Bind() failed" << endl;
//		goto close;
//	}
//	cout << "TCP/IP socket bound.\n";
//
//	if (listen(hSocket, 1) == SOCKET_ERROR) {
//		cerr << "Error listening on socket\n";
//		goto close;
//	}
//
//	{	// goto scope
//		cout << "Waiting for connection\n";
//		SOCKET hAccepted = SOCKET_ERROR;
//		while (hAccepted == SOCKET_ERROR) {
//			hAccepted = accept(hSocket, NULL, NULL);
//		}
//		cout << "Client connected\n";
//
//		int bytesSent;
//		//char recvbuf[32] = "";
//		int bytesRecv;
//		//bytesRecv = recv(hAccepted, recvbuf, 32, 0);
//		//cout << "Recv = " << bytesRecv << ": " << recvbuf << endl;
//
//
//		//char sendbuf[32] = "Goodbye";
//		//bytesSent = send(hAccepted, sendbuf, strlen(sendbuf) + 1, 0);
//		//cout << "Sent = " << bytesSent << " bytes" << endl;
//
//		for (;;) {
//			int i;
//			bytesRecv = recv(hAccepted, (char*)&i, 4, 0);
//			cout << "i = " << i << endl;
//			
//			//drop1
//			if (i >= 888888) break;
//			char* recvbuf = new char[i];
//
//			bytesRecv = recv(hAccepted, recvbuf, i, 0);
//			cout << "Recv = " << bytesRecv << ": " << recvbuf <<";" <<endl;
//
//			//drop2
//			//if (recvbuf[0] == 'd')
//			//	break;
//			delete recvbuf;
//		}
//
//		// terminate
//		cout << "Closing the accepted socket.\n";
//		closesocket(hAccepted);
//	}
//close:
//	cout << "Closing the listening socket.\n";
//	closesocket(hSocket);
//	WSACleanup();
//}