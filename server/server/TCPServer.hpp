#pragma once
// TCPServer
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include"Socket.hpp"
using namespace std;

#pragma comment (lib,"ws2_32.lib")






class TCPServer {
	//SOCKET hSocket;
	Socket s1;

	unsigned short PORT;
	//SOCKET hAccepted;

	Socket s2;

	Socket s3;

public:
	TCPServer(string ip) {
		PORT = 27015;
		// initialize WSA
		WSAData wsaData;
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			cerr << "WSAStartup failed: " << iResult << endl;
		}

		// Create the TCP socket
		s1.create();

		// Create the server address
		sockaddr_in serverAddress = { 0 };
		serverAddress.sin_family = AF_INET;
		serverAddress.sin_port = htons(PORT);
		 
		inet_pton(AF_INET, ip.c_str(), &(serverAddress.sin_addr));

		// bind the socket
		s1._bind(serverAddress);
	}

	~TCPServer() {
		stopListening();
	}


	void startListeningSingle() {
		if (s1._listen(10))
		{
			for (;;)
			{

				thread rcvThread(&Socket::wait_receive, &s2, ref(s1));

				thread sendThread(&Socket::wait_send, &s2, ref(s1));

				//thread t2([]() {for (;;) {
				//	cout << "shoot" << endl;
				//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				//}});


				//thread t1([]() {for (;;) {
				//	cout << "1" << endl;
				//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				//}});


				rcvThread.join();
				sendThread.join();

				//t2.join();
				//t.join();


				//t1.join();
			}
		}
	}

	void grep() {
		cout << "grepping" << endl;
	}

	void stopAccepted() {
		// terminate
		cout << "Closing the accepted socket.\n";
		closesocket(s2._socket_IN);


	}


	void stopListening() {
		stopAccepted();
		cout << "Closing the listening socket.\n";
		closesocket(s1._socket_IN);
		WSACleanup();
	}
};