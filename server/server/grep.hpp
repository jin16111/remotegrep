#pragma once
/*

Brief	Grepper takes a path, recursive scan the path, and generate a report
*/
#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <string>
#include <fstream>
#include <filesystem>

#include <Windows.h>
#include <algorithm>
#include <cassert>
#include <numeric>
#include <mutex>

#include <regex>
#include <algorithm>
#include <queue>

namespace fs = std::filesystem;
using namespace std;

class report_item_line
{
public:
	string line;
	unsigned line_num;
	unsigned match_num;

	report_item_line(unsigned ln, string l, unsigned match) : line_num(ln), line(l), match_num(match) {}
 
	string toString() const;

	bool operator<(report_item_line const& rhs) const;
};

class report_item
{
public:
	string file_name;
	vector<report_item_line> lines;
	bool operator<(report_item const& rhs) const;
	string toString() const;
};

class file_package
{
public:
	using it = vector<fs::directory_entry>::const_iterator;
	it begin, end;
	vector<report_item> reports;
	file_package(it b, it e) : begin(b), end(e) {}
};

class line_package
{
public:
	int begin, end;
	vector<report_item_line> reportItemLines;
	vector<string>* linesPtr;
	line_package(int b, int e, vector<string>* ptr) : begin(b), end(e), linesPtr(ptr) {}
};

class grepper
{
public:
	atomic<unsigned> _file_count;
	atomic<unsigned> _total_count;
	vector<report_item> _report;
	string _root_path;
	regex _pattern;
	regex reg_extensions;
	string _extension;
	bool _verbose;

	queue<string>* _sendStack;

	//constructor
	grepper(string p, regex r, regex e, bool v, queue<string>* sendStack);

	//grep
	void grep_cpp11();

	//scan files in package
	void scan_file_package_cpp11(file_package& filePackage);

	//scan a lines in package
	void san_line_package_cpp11(string file_name, line_package& package);

	//get frequency of a pattern in str
	unsigned get_frequency(string const& str, regex const& wordRgx);

	//locks verbose using mutex
	void verbose_cpp11(string msg);

	//print the report
	void print_report();

	//sort report first by file, then by line number
	void sort_report();
};