#pragma once
// TCPServer
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <vector>
#include <queue>
#include "grep.hpp"

using namespace std;

#pragma comment (lib,"ws2_32.lib")





class Socket {
public:
	SOCKET _socket_IN;

	SOCKET _socket_OUT;
	bool on = true;

	queue<string> sendStack;

	void create(const char* ip = "127.0.0.1", unsigned short port = 27015) {
		_socket_IN = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}

	//bind 
	bool _bind(sockaddr_in& serverAddress) {
		// bind the socket
		if (bind(_socket_IN, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
			cerr << "Bind() failed" << endl;
			close_server();
			return false;
		}
		cout << "TCP/IP socket bound.\n";
	}

	//listen
	bool _listen(int numClients = 10) {
		if (listen(_socket_IN, numClients * 2) == SOCKET_ERROR) {
			cerr << "Error listening on socket\n";
			close_server();
			return false;
		}
		return true;
	}


	//wait
	void wait(Socket& s1) {
		cout << "Waiting for connection\n";
		_socket_IN = SOCKET_ERROR;
		while (_socket_IN == SOCKET_ERROR) {
			_socket_IN = s1._accept();
		}
		cout << "Client connected\n";
	}

	//wait
	void wait_receive(Socket& s1) {
		cout << "Waiting for client command console\n";
		_socket_IN = SOCKET_ERROR;
		while (_socket_IN == SOCKET_ERROR) {
			_socket_IN = s1._accept();
		}
		cout << "client command console connected\n";

		//s2.receive();
		thread t(&Socket::receive, this);
		t.join();
	}

	//wait
	void wait_send(Socket& s1) {
		cout << "Waiting for client receiver\n";
		_socket_OUT = SOCKET_ERROR;
		while (_socket_OUT == SOCKET_ERROR) {
			_socket_OUT = s1._accept();
		}
		cout << "client receiver connected\n";

		//thread tms(&Socket::_mock_send_server, this);
		//tms.join();

		_send_server();

		//for (size_t i = 0; i < 100; i++)
		//{
		//	string s = "hello";
		//	thread t(&Socket::_send, this, ref(s));
		//	t.join();
		//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		//}
	}

	//connect
	bool _connect(sockaddr_in& serverAddress) {
		return connect(_socket_IN, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR;
	}

	//accept
	SOCKET _accept() {
		return accept(_socket_IN, NULL, NULL);
	}

	//bytesRecv = recv(s2._socket, recvbuf, i, 0);
	//receive, return the bytes received
	void receive() {//vector<string> cmdPatterns = { "" }) {
		while (on) {
			int i;
			int bytesRecv = recv(_socket_IN, (char*)&i, 4, 0);
			cout << "i = " << i << endl;
			//drop1
			if (i == -1 || bytesRecv == -1)
			{
				break;
				//TODO drop??
			}

			char* recvbuf = new char[i];
			bytesRecv = recv(_socket_IN, recvbuf, i, 0);
			cout << "Recv = " << bytesRecv << ": " << recvbuf << ";" << endl;
			char cmd[4] = { 'c','m','d' };
			if (bytesRecv >= 4
				&& recvbuf[0] == cmd[0]
				&& recvbuf[1] == cmd[1]
				&& recvbuf[2] == cmd[2]
				) {
				string str(recvbuf);
				cout << "*** command caught: '" << str << "'" << endl;

				if (str == "cmd grep") {
					string arg_path = "C:\\local\\boost_1_71_0\\boost\\beast";
					string arg_pattern = "#if";
					string arg_extension = ".hpp";


					//regex pattern
					regex pattern(arg_pattern);

					//regex extensions
					arg_extension.erase(0, 1);											//remove the leading .
					std::replace(arg_extension.begin(), arg_extension.end(), '.', '|'); //replace . with |
					regex extensions(arg_extension, regex::icase);

					grepper g(arg_path, move(pattern), move(extensions), true, &sendStack);



					thread tg(&grepper::grep_cpp11, &g);//, ref(queueMtx));


					//thread tms(&Socket::_mock_send_server, this);
					//tms.join();


					tg.join();
				}

			}
			delete[]recvbuf;
		}
	}

	void _mock_send_server() {
		//while (!sendStack.empty()) {
		//	mutex queueMtx;
		//	lock_guard<mutex> lk(queueMtx);
		//	cout << sendStack.front();
		//	sendStack.pop();
		//}
		mutex queueMtx;
		while (true) {
			while (!sendStack.empty()) {
				mutex queueMtx;
				lock_guard<mutex> lk(queueMtx);
				cout << sendStack.front() << endl;
				sendStack.pop();
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		}
	}

	void _send_server() {
		mutex queueMtx;
		while (true) {
			while (!sendStack.empty()) {
				mutex queueMtx;
				lock_guard<mutex> lk(queueMtx);
				_send(sendStack.front());
				sendStack.pop();
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		}
	}

	//send
	void _send(string& msg) {
		int msgLngh = 0;
		msgLngh = msg.length() + 1;

		send(_socket_OUT, (char*)&msgLngh, sizeof(msgLngh), 0);

		char* sendbuf = new char[msgLngh];

		strcpy_s(sendbuf, msg.length() + 1, msg.c_str());	// or pass &s[0]
		//cout << "sendbuf = " << sendbuf << endl;
		//cout << "strlen(sendbuf) + 1 = " << strlen(sendbuf) + 1 << endl;
		int bytesSent = send(_socket_OUT, sendbuf, strlen(sendbuf) + 1, 0);

		delete[]sendbuf;
	}


	void _sendDrop() {
		int i = -1;
		send(_socket_IN, (char*)&i, sizeof(i), 0);
	}

	//operators
	void operator =(SOCKET rhs) {
		_socket_IN = rhs;
	}

	bool operator ==(SOCKET rhs) {
		return _socket_IN == rhs;
	}



	//stop listen
	//close
	void close() {
		closesocket(_socket_IN);
		WSACleanup();
	}


	//stop listen
	//close
	void close_server() {
		closesocket(_socket_IN);
		//WSACleanup();
	}
};
