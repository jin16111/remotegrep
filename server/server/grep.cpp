/*
author	Jinzhi Yang
Date	2019-11-02
Brief	implementation of grepper's methods
*/

#include "grep.hpp"
mutex console_mtx;

grepper::grepper(string p, regex r, regex e, bool v, queue<string>* sendStack) : _root_path(p), _pattern(r), reg_extensions(e), _file_count(0), _total_count(0), _verbose(v),_sendStack(sendStack) {}

void grepper::verbose_cpp11(string msg)
{
	lock_guard<mutex> lk(console_mtx);
	//cout << msg << endl;
	_sendStack->push(msg);
}

void grepper::san_line_package_cpp11(string file_name, line_package& package)
{
	for (auto i = package.begin; i < package.end; i++)
	{
		//unsigned freq = unsigned(countFreq_MT(pattern, (*package.linesPtr)[i]));

		unsigned freq = get_frequency((*package.linesPtr)[i], _pattern);
		if (freq != 0)
		{
			package.reportItemLines.push_back(report_item_line(i + 1, (*package.linesPtr)[i], freq));
			if (_verbose)
				verbose_cpp11("Matched " + to_string(freq) + ": \"" + file_name + "\" [" + to_string(i) + "] " + (*package.linesPtr)[i] + "\n");
		}
	}
}

unsigned grepper::get_frequency(string const& str, regex const& wordRgx)
{
	unsigned total = 0;
	auto words_itr = sregex_iterator(str.cbegin(), str.cend(), wordRgx);
	auto words_end = sregex_iterator();
	while (words_itr != words_end)
	{
		auto match = *words_itr;
		auto word = match.str();
		if (word.size() > 0)
			++total;
		words_itr = next(words_itr);
	}
	return total;
}

void  grepper::scan_file_package_cpp11(file_package& filePackage)
{
	for (auto it = filePackage.begin; it != filePackage.end; it++)
	{
		if (is_directory((*it).status()))
		{
			if (_verbose)
				grepper::verbose_cpp11("\nScanning " + (*it).path().string() + "\n");
		}
		else
		{

			//if extension matches the Regex, generate a ReportItem
			if (regex_match((*it).path().extension().u8string().erase(0, 1), reg_extensions))
			{
				{
					if (_verbose)
						grepper::verbose_cpp11("\nGrepping " + (*it).path().string() + "\n");
				}

				string line;

				report_item newReport;
				newReport.file_name = (*it).path().string();

				vector<string> lines;

				{
					ifstream file((*it).path());

					while (getline(file, line))
					{
						lines.push_back(move(line));
					}
				}

				//get num of threads
				SYSTEM_INFO si;
				GetSystemInfo(&si);
				auto nThreads = si.dwNumberOfProcessors; //dw number is DWORD, DWORD is unsigned long

				//process the lines using MT
				size_t nElements = lines.size();

				//Create tasks
				vector<line_package> linePackages;

				//generate line groups
				for (size_t i = 0; i < nThreads; i++)
				{
					size_t startOff = size_t(i * nElements / nThreads);
					size_t endOff = size_t((i + 1) * nElements / nThreads);
#ifdef _DEBUG
					cout << "line thread ->" << i << "  -> [" << startOff << "  |  " << endOff << ")" << endl;
#endif // DEBUG

					linePackages.push_back(move(line_package(startOff, endOff, &lines)));
				}

				//put groups into threads
				vector<thread> threads;
				for (auto& linepackage : linePackages)
				{
					threads.push_back(move(thread(&grepper::san_line_package_cpp11,this ,(*it).path().string(), ref(linepackage))));
				}

				//launch
				for (auto& t : threads)
				{
					t.join();
				}

				for (auto& linepackage : linePackages)
				{
					if (!linepackage.reportItemLines.empty())
					{
						newReport.lines.insert(newReport.lines.end(), linepackage.reportItemLines.begin(), linepackage.reportItemLines.end());
					}
				}

				if (!newReport.lines.empty())
				{
					filePackage.reports.push_back(newReport);
				}
			}
		}
	}
}





void grepper::print_report()
{
	cout << "Grep Report:\n\n";
	for (auto const& report : _report)
	{
		cout << report.toString() << endl;
	}
}

void grepper::grep_cpp11()
{
	vector<fs::directory_entry> directories;

	for (auto& p : fs::recursive_directory_iterator(_root_path))
	{
		directories.push_back(p);
	}

	size_t nElements = directories.size();

	//get num of threads
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	auto nThreads = si.dwNumberOfProcessors; //dw number is DWORD, DWORD is unsigned long

	//Create tasks
	vector<file_package> packages;

	// make groups based on the number of directory entries
	for (size_t i = 0; i < nThreads; i++)
	{
		size_t startOff = size_t(i * nElements / nThreads);
		size_t endOff = size_t((i + 1) * nElements / nThreads);
#ifdef _DEBUG
		cout << "file thread ->" << i << "  -> [" << startOff << "  |  " << endOff << ")" << endl;
#endif
		packages.push_back(move(file_package(directories.begin() + startOff, directories.cbegin() + endOff)));
	}

	//put groups into threads
	vector<thread> threads;
	for (auto& package : packages)
	{
		threads.push_back(move(thread(&grepper::scan_file_package_cpp11, this,ref(package))));
	}

	//launch
	for (auto& t : threads)
	{
		t.join();
	}

	//concatenate reports
	for (auto const& package : packages)
	{
		_report.insert(_report.end(), package.reports.begin(), package.reports.end());
	}

	//get the total match num
	for (auto& item : _report)
	{
		for (auto const& l : item.lines)
		{
			_total_count += l.match_num;
		}
	}
	sort_report();
}

void grepper::sort_report()
{
	// sort lines
	for (auto& item : _report)
	{
		sort(item.lines.begin(), item.lines.end());
	}
	//sort items
	sort(_report.begin(), _report.end());
}


string report_item::toString() const
{
	string result = "";
	result += "\"" + file_name + "\"" + "\n";
	for (auto& item : lines)
	{
		result += item.toString() + "\n";
	}
	result += "\n";
	return result;
}

bool report_item::operator<(report_item const& rhs) const
{
	return this->file_name < rhs.file_name;
}


string report_item_line::toString() const
{
	string matchNumString = "";
	if (match_num > 1)
		matchNumString = ":" + to_string(match_num);
	return "[" + to_string(line_num) + matchNumString + "] " + line;
}

bool report_item_line::operator<(report_item_line const& rhs) const
{
	return this->line_num < rhs.line_num;
}