#pragma once
/*
Author	Jinzhi Yang
Date	2019.11.01
Brief	ThreadPool manages threads, user can define the number of threads and push tasks into threads to execute.
		Both Win32 and C++11 threading are supported
*/

#ifdef _WIN32_but_I_prefer_cpp11
#include <windows.h>
#include <chrono>
#include <functional>
#include <iostream>
#include <queue>
#include <string>
#include <vector>
#include <future>


class CriticalSection {
	CRITICAL_SECTION m_cs;
public:
	CriticalSection() { InitializeCriticalSection(&m_cs); }
	~CriticalSection() { DeleteCriticalSection(&m_cs); }

private:
	void Enter() { EnterCriticalSection(&m_cs); }
	void Leave() { LeaveCriticalSection(&m_cs); }

	friend class CSLock;
};
class CSLock {
	CriticalSection& m_csr;
public:
	CSLock(CriticalSection& cs) : m_csr(cs) {
		m_csr.Enter();
	}
	~CSLock() {
		m_csr.Leave();
	}
};

#else
#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#endif

using namespace std;
class ThreadPool {
#ifdef _WIN32_but_I_prefer_cpp11
private:

	vector<HANDLE> threads;
	static bool workTime;
	static HANDLE wakeEvent;
	static CriticalSection taskMtx;
	static queue<function<void()>> tasks;
public:
	ThreadPool(size_t nThreads);
	~ThreadPool();
	template<class F, class... Args>
	inline future<typename result_of<F(Args...)>::type> push(F&& f, Args&&... args) {
		auto task = make_shared<packaged_task<result_of<F(Args...)>::type()>>(bind(forward<F>(f), forward<Args>(args)...));
		future<result_of<F(Args...)>::type> res = task->get_future();

		{CSLock lk(taskMtx);

		if (workTime)
			tasks.push([task]() { (*task)(); });
		}
		SetEvent(wakeEvent);
		return res;
	}
	// perform tasks in a thread
	DWORD static WINAPI  perform_task(LPVOID);

#else
private:
	vector<thread> _threads;
	mutex _taskMtx;
	condition_variable _wakeCV;
	bool _possibleMoreWork;
	queue<function<void()>> _tasks;
public:
	ThreadPool(size_t nThreads);
	~ThreadPool();
	//add task to task queue
	template<class F, class... Args>
	inline future<typename result_of<F(Args...)>::type>  push(F&& f, Args&&... args) {
		auto task = make_shared<packaged_task<result_of<F(Args...)>::type()>>(bind(forward<F>(f), forward<Args>(args)...));
		future<result_of<F(Args...)>::type> res = task->get_future();

		{unique_lock<mutex> lock(_taskMtx);

		if (_possibleMoreWork)
			_tasks.push([task]() { (*task)(); });
		}
		_wakeCV.notify_one();
		return res;
	}
	// perform tasks in a thread
	void perform_task();
#endif
};
