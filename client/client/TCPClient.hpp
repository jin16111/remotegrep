#pragma once

// TCPClient
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>
#include <string>
#include "Socket.hpp"
using namespace std;

#pragma comment (lib,"ws2_32.lib")

class TCPClient {
public:
	bool connected;
	//SOCKET _hSocket;

	Socket s1;

	unsigned short PORT;
	TCPClient(string ip) {
		PORT = 27015;
		cout << "TCPClient\n";
		connectServer(ip);
	}

	~TCPClient() {
		disconnectServer();
	}

	bool connectServer(string ip) {
		cout << "connecting\n";
		// initialize WSA
		WSAData wsaData;
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			cerr << "WSAStartup failed: " << iResult << endl;
			return EXIT_FAILURE;
		}
		//_hSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		s1.create();

		// Create the server address
		sockaddr_in serverAddress = { 0 };
		serverAddress.sin_family = AF_INET;
		serverAddress.sin_port = htons(PORT);
		//serverAddress.sin_addr.s_addr = inet_addr("127.0.0.1"); // old
		inet_pton(AF_INET, ip.c_str(), &(serverAddress.sin_addr));

		if (s1._connect(serverAddress)) {
			cerr << "connect() failed" << endl;
			disconnectServer();
			return false;
		}


		cout << "connected to server ip:port" << endl;
		connected = true;
		return true;
	}

	void sendMsg(string msg = "") {
		if (msg == "drop") {
			disconnectServer();
		}
		else
			s1._send(msg);
	}

	void drop() {
		s1._sendDrop();
	}

	void disconnectServer() {
		drop();
		s1.close();
		//WSACleanup();
		cout << "disconnected" << endl;
		connected = false;
	}
};